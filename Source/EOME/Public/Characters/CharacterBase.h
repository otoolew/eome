// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "CharacterBase.generated.h"

class UCameraComponent;
class USpringArmComponent;

UCLASS()
class EOME_API ACharacterBase : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACharacterBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual FVector GetPawnViewLocation() const override;

	/************************************************************************/
/* Mesh                                                                 */
/************************************************************************/
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Mesh")
	USkeletalMeshComponent* FPPMesh;

	UPROPERTY(EditDefaultsOnly, Category = "Mesh")
	FName HeadSocketName;

	UPROPERTY(EditDefaultsOnly, Category = "Mesh")
	FName GripSocketName;
public:
	FORCEINLINE class USkeletalMeshComponent* FPPMeshComponent() const { return FPPMesh; }

/************************************************************************/
/* Camera                                                               */
/************************************************************************/
protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UCameraComponent* CameraComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USpringArmComponent* SpringArmComp;

/************************************************************************/
/* Movement                                                             */
/************************************************************************/
protected:
	UPROPERTY(BlueprintReadOnly, Category = "Movement")
	bool MoveSpeed;

	UPROPERTY(BlueprintReadOnly, Category = "Movement")
	float MoveDirection;

public:
	// Movement
	UFUNCTION(BlueprintCallable, Category = "Movement")
	float GetSpeed() const;

	UFUNCTION(BlueprintCallable, Category = "Movement")
	float GetDirection() const;

	virtual void MoveForward(float Value);

	virtual void MoveRight(float Value);

	// Crouching
	UFUNCTION(BlueprintCallable, Category = "Movement")
	virtual void StartCrouch();

	UFUNCTION(BlueprintCallable, Category = "Movement")
	virtual void EndCrouch();

/************************************************************************/
/* Weapon                                                               */
/************************************************************************/
//protected:
//	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
//		TSubclassOf<AWeaponBase> StartingWeapon;
//
//	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
//		FName WeaponAttachSocketName;
//
//	AWeaponBase* EquippedWeapon;
};

