// Copyright Epic Games, Inc. All Rights Reserved.

#include "EOME.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, EOME, "EOME" );
